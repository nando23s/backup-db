#!/usr/bin/env bash

#############################################################
#                                                           #
# MariaDb/MySQL backup script                               #
#                                                           #
# Author: Sergey Basov https://gitlab.com/kukymbr           #
# Based on http://hudson.su/2011/03/03/mysql-backup-script  #
# Author of original script: dmitry.bykadorov@gmail.com     #
# Licensed under the MIT license. See the LICENSE file.     #
#                                                           #
# Rename file backup-db.conf.dist to backup-db.conf         #
# Renombra el archivo backup-db.conf.dist a backup-db.conf  #
# and set there your database connection options before run #
# y establece la conexión a la Base de Datos antes de usar  #
#                                                           #
#############################################################

## Functions

# Get real path of filename
get_real_path() {

	# $1: relative filename
	filename=$1

	# Try to use readlink. Wont work on MacOS.
	real=$(readlink -f "${filename}" 2>/dev/null)
	res=$?
	if [ "$res" -eq 0 ]; then
		echo "$real"
		return 0
	fi

	# Try to use realpath if it is installed.
	real=$(realpath "${filename}" 2>/dev/null)
	res=$?
	if [ "$res" -eq 0 ]; then
		echo "$real"
		return 0
	fi

	parentdir=$(dirname "${filename}")

	if [ -d "${filename}" ]; then
		echo "$(cd "${filename}" && pwd)"
		return 0
	elif [ -d "${parentdir}" ]; then
		echo "$(cd "${parentdir}" && pwd)/$(basename "${filename}")"
		return 0
	fi

	echo "${filename}"
	return 1
}

# Echo to stderr
echo_err() {
	echo
	echo -e "$@" 1>&2;
}

## Variables

# Absolute path to this script
SELF=$(get_real_path $0)

# Path to config file
FILE_CONF="$1"

# Report labels
LABEL_OK="\033[0;32m[OK]\033[0m"
LABEL_FAIL="\033[0;31m[FAIL]\033[0m"
LABEL_WARN="\033[0;33m[WARN]\033[0m"

# Current Date & time values
TIME_YEAR=$(date +%Y)
TIME_MONTH=$(date +%m)
TIME_DAY=$(date +%d)
TIME_HOURS=$(date +%H)
TIME_MINUTES=$(date +%M)



## Pre-conditions

if [ ! -f "$FILE_CONF" ]
then
	echo_err "$LABEL_FAIL\tConfig file $FILE_CONF does not exist"
	exit 1
fi

. "$FILE_CONF"



## Execution

# Full path to store dump in
TARGET_PATH="$TARGET_DIR/${TIME_YEAR}${TIME_MONTH}${TIME_DAY}";

mkdir -p "$TARGET_PATH";

if [ ! -d "$TARGET_PATH" ]; then
	echo_err "$LABEL_FAIL\tFailed to create target path: $TARGET_PATH"
	exit 2
fi

echo -e "\n$(tput bold)MariaDB/MySQl backup tool"
echo "Config file: $FILE_CONF"
echo -e "Target dir: $TARGET_PATH$(tput sgr0)\n"


# Temporary file with databases list
if [ "$OSTYPE" == "linux-gnu" ]; then
	FILE_DB_LIST=$(mktemp --tmpdir=/tmp backup-db.XXXXX)
elif [ "$OSTYPE" == "darwin"* ]; then
	FILE_DB_LIST=$(mktemp -q /tmp/backup-db.XXXXX)
elif [ "$OSTYPE" == "cygwin" ]; then
  echo_err "$LABEL_FAIL\tOperating System Not supported"
	exit 3
elif [ "$OSTYPE" == "msys" ]; then
	echo_err "$LABEL_FAIL\tOperating System Not supported"
	exit 4
elif [ "$OSTYPE" == "win32" ]; then
	echo_err "$LABEL_FAIL\tOperating System Not supported"
	exit 5
elif [ "$OSTYPE" == "freebsd"* ]; then
	echo_err "$LABEL_FAIL\tOperating System Not supported"
	exit 6
else
	echo_err "$LABEL_FAIL\tUnknown Operating System"
	exit 7
fi

mysql -sN -h "$DB_HOST" -u "$DB_USER" -p"$DB_PASSWORD" -e "SHOW DATABASES;" > "$FILE_DB_LIST"
res=$?

if [ "$res" -ne 0 ]; then
	echo_err "$LABEL_FAIL\tFailed to connect to database"
	exit 8
fi

cd "$TARGET_PATH"

BACKUP_COUNT=${#BACKUP_DATABASES[@]}

db_res_count=0

for db_name in $(cat "$FILE_DB_LIST")
do
	backup=0
	# Backup databases from BACKUP_DATABASES list
	for backup_name in $BACKUP_DATABASES
	do
		if [ "$db_name" = "$backup_name" ]; then
			backup=1
			break
		fi
	done

	if [ "$backup" -eq 1 ]; then
		file_sql="$db_name.$TIME_YEAR$TIME_MONTH$TIME_DAY-$TIME_HOURS$TIME_MINUTES.sql"
		file_gzip="$file_sql.gz"

		/usr/bin/mysqldump --single-transaction -h "$DB_HOST" --databases "$db_name" -u "$DB_USER" --password="$DB_PASSWORD" "$ADD_DUMP_OPTIONS" > "$file_sql"

		res=$?
		if [ "$res" -eq 0 ]; then

			echo -e "$LABEL_OK\t$db_name: dump created"
			/bin/gzip -9 -f "$file_sql" > "$file_gzip"

			res=$?
			if [ "$res" -eq 0 ]; then
				echo -e "$LABEL_OK\t$db_name: dump saved to archive"
				db_res_count=$((db_res_count+1))
			else
				echo -e "$LABEL_WARN\t$db_name: failed to archive dump"
			fi

		else
			echo -e "$LABEL_WARN\t$db_name: failed to create dump"
		fi
	fi

done

del_res_count=0

# Delete old files
if [ "$DAYS_LIFETIME" -ne 0 ]; then
	find "$TARGET_DIR/" -maxdepth 4 -type f -name "*.sql.gz" -mtime +"$DAYS_LIFETIME" | while read file_to_del; do
		if [ ! -z "$file_to_del" ]; then
			/bin/rm -f "$file_to_del"
			res=$?
			if [ "$res" -eq 0 ]; then
				echo -e "$LABEL_OK\t$file_to_del deleted"
				del_res_count=$((del_res_count+1))
			else
				echo -e "$LABEL_WARN\t$file_to_del: failed to delete old file"
			fi
		fi
	done
fi

/bin/rm "$FILE_DB_LIST"

res=$?
if [ "$res" -ne 0 ]
then
	echo -e "$LABEL_WARN\tfailed to delete $FILE_DB_LIST"
fi

echo -e "\n$LABEL_OK\t$(tput bold)Done. $db_res_count dump(s) created, $del_res_count deleted$(tput sgr0)\n"
exit 0
