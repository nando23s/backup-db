# Script en BASH para respaldar Bases de Datos MySQL/MariaDB

Script en BASH para Respaldar MySQL/MariaDB
Basado en [esté script](http://hudson.su/2011/03/03/mysql-backup-script).

Autor Original: [Sergey Basov](https://gitlab.com/kukymbr).

## Uso

Renombra el archivo `backup-db.conf.dist` a `backup-db.conf` establece la conexión a la Base de Datos, el directorio donde guardar los respaldos y demás opciones.

	$ mv ./backup-db.conf.dist ./backup-db.conf

Haz `backup-db.sh` ejecutable

    $ chmod u+x ./backup-db.sh

y ejecútalo.

	$ ./backup-db.sh backup-db.conf

Ve tu `$TARGET_DIR` de `backup-db.conf` para ver los archivos resultantes.

